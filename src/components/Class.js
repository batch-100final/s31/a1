// base imports
import React from 'react';

export class Class extends React.Component{
	render(){
		return(
			<p>This is a class components</p>
		)
	}
}

export class ClassComp extends React.Component{
	render(){
		return(
			<p>This is another class</p>
		)
	}
}

