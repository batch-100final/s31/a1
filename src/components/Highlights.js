// base imports
import React from 'react';

// bootstrap components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

// export default allows the function to be used in other files
export default function Highlights(){
	return(
		<Row>
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title><h2>Learn from Home</h2></Card.Title>
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper. </p>
					</Card.Body>
				</Card>
			</Col>
			{/* 1st Card end */}
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title><h2>Learn from Home 2</h2></Card.Title>
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper. </p>
					</Card.Body>
				</Card>
			</Col>
			{/* 2nd Card end */}
			<Col cs={12} md={4}>
				<Card className="card-highlight">
					<Card.Body>
						<Card.Title><h2>Learn from Home 3</h2></Card.Title>
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia suscipit ullamcorper. </p>
					</Card.Body>
				</Card>
			</Col>
			{/* 3rd card end */}
		</Row>
	)
}