// base imports
import React from'react';

// app components
import FC from './components/Function';
import {Class, ClassComp} from './components/Class';

import Classprops from './Classprops';

function App(){
	return(
		<div>
			<h1> Hello!</h1>
			<Classprops name="Ken" age="19" />
			<Classprops name="Ken2" age="18" />
			<Classprops name="Ken3" />
			<h2>This is the components</h2>
			<FC />
			<Class />
			<ClassComp />
		</div>
	);
}

export default App;