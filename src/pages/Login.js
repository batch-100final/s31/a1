// base imports
import React, { useState,useEffect } from 'react';

// bootstrap
import {Form, Button, Container} from 'react-bootstrap';

// export default allows the function to be used in other files
export default function Login() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function login(e) {
		// prevents page redirection
		e.preventDefault(); 

		// clear the input fields
		setEmail('');
		setPassword('');

		alert("Login successful. You are now logged in.");
	}

	useEffect(() => {
		let xEmail = email;
		let xPassword = password;

		if (xEmail !== '' && xPassword !== '') {
			setIsActive(false);
		} else {
			setIsActive(true);
		}
	}, [email, password])


	return(
		<Container>
			<h3>Login</h3>
				<Form onSubmit={login}>
					<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
					type="email"
					value={email}
					placeholder="Enter email" 
					onChange={(e) => setEmail(e.target.value)} 
					required
				/> 
				</Form.Group> 

				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
					type="password" 
					value={password}
					placeholder="Enter Password" 
					onChange={(e) => setPassword(e.target.value)} 
					required
				/> 
				</Form.Group> 

				<Button className="bg-primary" type="submit" disabled={isActive}>Submit</Button>

				</Form>
		</Container>
	)
}