// Base imports
import React, {Fragment} from 'react';

import { Container } from 'react-bootstrap';
// app components
import Navbar from './components/Navbar';

// page components
// import Home from './pages/Home';
// import Courses from './pages/Courses';
// import Register from './pages/Register';
import Login from './pages/Login';

export default function App(){
	return(
		<Fragment>
			<Navbar />
			<Container className="my-5">
				<Login />
			</Container>
		</Fragment>
	);
}

